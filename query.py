from SPARQLWrapper import SPARQLWrapper, JSON


def sparql_query(query, endpoint, output=JSON, fn=lambda _:_["results"]["bindings"]):
    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query)
    sparql.setReturnFormat(output)
    return fn(sparql.query().convert())