A very simple project showing how to exercise your rights with respect to advertisers

Click below to test it out!

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/PersonalDataIO%2Fgdpr-rights-notebook/master?filepath=GDPR_rights_assistant.ipynb)