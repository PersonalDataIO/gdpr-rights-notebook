# pip install sparqlwrapper
# https://rdflib.github.io/sparqlwrapper/

import endpoints, entities, query


query_templates = { 
    "controller":
    """SELECT ?email ?name WHERE {{
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }}
      wd:{target} p:{email} ?emailStatement.
      ?emailStatement ps:{email} ?email.
      {{
        {{?emailStatement pq:{use} wd:{right_of_access}}}
        UNION
        {{?emailStatement pq:{of} wd:{dpo}}}
      }}.
      wd:{target} rdfs:label ?name
      FILTER((LANG(?name)) = "en")
    }}
    LIMIT 100
    """,    # This searches all items with an email listed that is marked as for "right of access" use
    "identifier_items":
    """SELECT ?identifierLabel WHERE {{
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }}
      wd:{target} p:{uses} ?usesStatement.
      ?usesStatement ps:{uses} ?identifier.
      ?usesStatement pq:{use} wd:{right_of_access}
    }}
    LIMIT 100
    """,
    "requested_items":
    """SELECT DISTINCT ?requestedLabel WHERE {{
      SERVICE wikibase:label {{ bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }}
      wd:{target} p:{uses} ?usesStatement.
      ?usesStatement ps:{uses} ?requested.
      ?usesStatement pq:{fabrication_method} ?method.
      ?method wdt:{subclass_of}*  wd:{individual_data}.
    }}
    LIMIT 100
    """,
}


def get_extractor(template_id, fn=lambda _:_, unique=False):
    query_template = query_templates[template_id]
    template_target = query_template.format(
        target="{target}",
        **entities.ids
    )

    def extractor(target):
        query_str = query_template.format(target=target, **entities.ids)
        def inner():
            results = query.sparql_query(
                query_str,
                endpoints.wikidata
            )
            results_mapped = list(map(fn, results))
            if unique:
                assert(len(results) == 1)
                return results_mapped[0]
            return results_mapped
        inner.query = query_str
        return inner

    extractor.query = template_target
    return extractor

get_controller = get_extractor("controller",
                          fn = lambda _: \
                          {"email": _["email"]["value"].lstrip("mailto:"),
                           "name": _["name"]["value"]},
                          unique=True)
get_identifiers = get_extractor("identifier_items", fn=lambda _:_["identifierLabel"]["value"])
get_requested_data = get_extractor("requested_items", fn=lambda _:_["requestedLabel"]["value"])
